from infoII_tp4_ex3 import File


file_prioritaire = File()


file_prioritaire.enqueue((4, "Nathan"))
file_prioritaire.enqueue((2, "Julia"))
file_prioritaire.enqueue((1, "Amandine"))
file_prioritaire.enqueue((3, "Mathias"))

file_prioritaire.afficher()

personne_prioritaire = file_prioritaire.dequeue()
print(f"\nPersonne prioritaire : {personne_prioritaire}") 

file_prioritaire.afficher()

