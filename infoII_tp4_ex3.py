class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        if self.is_empty():
            self.items.insert(0, item)
        else:
            isInserted = False
            for i, current_item in enumerate(self.items):
                
                if item[0] < current_item[0]:
                    
                    self.items.insert(i, item)
                    isInserted = True
                    break
            if not isInserted:
                
                self.items.append(item)

    def dequeue(self):
        return self.items.pop(0)

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)
    
    def afficher(self):
        for item in self.items:
            print(item)
    
    
  

